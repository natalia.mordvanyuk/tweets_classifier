import os

from tweets_classifier.estimators import train_dataset, test_dataset

text_col = "text"
y_col = "sentiment"
other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                          'GradientBoostingClassifier']
input_path = "data" + os.sep + "train_p.csv"
input_sep = ";"
cols_to_delete = ["textID", "selected_text"]

train_dataset(input_path, input_sep, text_col, y_col, model_names, other_x_cols, cols_to_delete)

input_path_test = "data" + os.sep + "test.csv"
input_sep_test = ","
test_dataset(input_path_test, input_sep_test)
