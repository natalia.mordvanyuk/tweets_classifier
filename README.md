# tweets_classifier tool

tweets_classifier is a tool to clean and classify a Twitter dataset in csv format. 

## Usage

Import the required functions:

```sh
from tweets_classifier.estimators import train_dataset, test_dataset
```

Initialize you variables:

```sh
# Initialize you variables
input_path = "data/train.csv" # dataset path
input_sep = "," # dataset separator
text_col = "text" # name of the text column in your dataset
y_col = "sentiment"  # name of the target column in your dataset
```
Sometimes it might be interesting to preserve emojis, or text hashtags, because they can be discriminatory elements of sarcasm. These are all the possible new columns that can be derived from the text column. Choose the columns you want to create.
```sh
other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
```
Choose the classifiers to test your model.
```sh
model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                          'GradientBoostingClassifier']
```
Select the columns you want to delete.
```sh
cols_to_delete = ["textID", "selected_text"]
```
The train function will train your dataset with all selected models, saving the best model under the name best. Note that in this function the train data will be divided into train and validation sets to test the different models.

```sh
train_dataset(input_path, input_sep, text_col, y_col, model_names, other_x_cols, cols_to_delete)
```
Specify you test dataset path, and call test_dataset. The test will run on the best model of the train.

```sh
input_path_test = "data" + os.sep + "test.csv"
input_sep_test = ","
test_dataset(input_path_test, input_sep_test)
```
Retrieve your results from the created results folder.

## Features

Currently supports cleaning, tokenizing and storing as separate fields into the dataset:
- URLs
- Hashtags
- Mentions
- Reserved words (RT, FAV)
- Emojis
- Smileys
- Numbers

The library that transforms text to vector is spacy (concreatly en_core_web_lg, that uses the large pretrained models)

Currently supports the following encoders, to transform categorical columns into numerical ones:
- OneHotEncoder
- OrdinalEncoder (for class, or scalar)

Currently supports the following classifiers:
- SVC
- MLP
- 11-NN
- RandomForest
- GaussianProcessClassifier
- GaussianNB
- GradientBoostingClassifier

And the following evaluation metrics:
- Balanced accuracy
- Precision  (per class, macro avg, weighted avg)
- Recall  (per class, macro avg, weighted avg)
- F1-score (per class, macro avg, weighted avg)
- Support per class

Files:
- CSV file support

## Tech

tweets_classifier uses a number of open source projects to work properly:
- [setuptools] - a collection of enhancements to the Python distutils that allow developers to more easily build and distribute Python packages, especially ones that have dependencies on other packages
- [numpy] - NumPy is a Python library used for working with arrays.
- [scipy] - is a collection of mathematical algorithms and convenience functions built on the NumPy extension of Python.
- [scikit-learn] - is a key library for the Python programming language that is typically used in machine learning projects
- [pandas] - is a software library written for the Python programming language for data manipulation and analysis. 
- [tweet-preprocessor] - is a preprocessing library for tweet data written in Python
- [category_encoders] - is a set of scikit-learn-style transformers for encoding categorical variables into numeric with different techniques.

## Installation
tweets_classifier can be installed with the following command, where x.x.x is the corresponding latest version:
```sh
pip install -i https://test.pypi.org/simple/ tweets-sentiment-natalia.mordvanyuk==0.0.4
```
Since TestPyPI doesn’t have the same packages as the live PyPI, it’s possible that attempting to install dependencies may fail or install something unexpected. For this reason make sure of installing the following packages:
```sh
pip install -U pip setuptools wheel
pip install numpy
pip install scipy
pip install scikit-learn
pip install pandas
pip install tweet-preprocessor
pip install category_encoders
pip install PyInquirer
pip install -U spacy
python -m spacy download en_core_web_lg
```
## License

MIT

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen.)

[setuptools]: <https://pypi.org/project/setuptools/>
[numpy]: <https://numpy.org/doc/stable/user/whatisnumpy.html>
[scipy]: <https://scipy.org/>
[scikit-learn]: <https://scikit-learn.org/stable/>
[pandas]: <https://pandas.pydata.org/>
[tweet-preprocessor]: <https://pypi.org/project/tweet-preprocessor/>
[category_encoders]: <https://contrib.scikit-learn.org/category_encoders/>