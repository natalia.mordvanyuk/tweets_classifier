import os
from time import time, ctime

import numpy as np
import pandas as pd
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import classification_report

from tweets_classifier.model_wrapper import Model


def store_metrics(model: Model, y_test: np.array, y_pred: np.array, classes_list: list[np.int32], is_train: bool = True):
    report = classification_report(y_test, y_pred, target_names=classes_list, output_dict=True, zero_division=1)
    df_report = pd.DataFrame(report).transpose()
    balanced_acc = balanced_accuracy_score(y_test, y_pred)

    if is_train:
        df_report.to_csv("results"+os.sep+"classif_report_"+model.name + "_train.csv")
        with open("results" + os.sep + "accuracies_train.csv", 'a') as file1:
            t = time()
            file1.write("\n"+str(ctime(t)) + ";" + model.name + ";" + str(balanced_acc))
    else:
        df_report.to_csv("results"+os.sep+"classif_report_"+model.name + "_test.csv")
        with open("results" + os.sep + "accuracies_test.csv", 'a') as file1:
            t = time()
            file1.write("\n"+str(ctime(t)) + ";" + model.name + ";" + str(balanced_acc))
