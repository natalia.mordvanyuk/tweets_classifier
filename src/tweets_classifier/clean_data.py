import os
from typing import Optional

import pandas as pd
import preprocessor as p
from preprocessor.parse import ParseResult


def fill_row(row: pd.Series, parsed_tweet: ParseResult, new_columns: list[str]) -> None:
    """
     Fills the row with tokens from the parsed_tweet, that are in the new_columns list
    :param row: pandas series
    :param parsed_tweet: a parsed tweet
    :param new_columns: possible columns to fill. Possible values: p.OPT.URL,p.OPT.MENTION, p.OPT.HASHTAG,p.OPT.RESERVED,p.OPT.EMOJI,p.OPT.SMILEY,p.OPT.NUMBER

    :return: None
    :rtype: None
    """
    for col in new_columns:
        if col == p.OPT.URL:
            if parsed_tweet.urls:
                for parse_item in parsed_tweet.urls:
                    row[col] = parse_item.match
        elif col == p.OPT.MENTION:
            if parsed_tweet.mentions:
                for parse_item in parsed_tweet.mentions:
                    row[col] = parse_item.match
        elif col == p.OPT.HASHTAG:
            if parsed_tweet.hashtags:
                for parse_item in parsed_tweet.hashtags:
                    row[col] = parse_item.match
        elif col == p.OPT.RESERVED:
            if parsed_tweet.reserved:
                for parse_item in parsed_tweet.reserved:
                    row[col] = parse_item.match
        elif col == p.OPT.EMOJI:
            if parsed_tweet.emojis:
                for parse_item in parsed_tweet.emojis:
                    row[col] = parse_item.match
        elif col == p.OPT.SMILEY:
            if parsed_tweet.smileys:
                for parse_item in parsed_tweet.smileys:
                    row[col] = parse_item.match
        elif col == p.OPT.NUMBER:
            if parsed_tweet.numbers:
                for parse_item in parsed_tweet.numbers:
                    row[col] = parse_item.match


def clean_dataset(input_path: str, column_name: str, input_sep: str = ",", output_path: Optional[str] = None, *,
                  cols_to_delete: Optional[list[str]] = None, clean_options: Optional[list[str]] = None,
                  new_columns: Optional[list[str]] = None) -> str:
    """
     Cleans irrelevant information from a .csv or .txt datasets.
    :param input_path: path to the .csv or .txt dataset.
    :param column_name: column name of the dataset corresponding to the tweet text to clean. Only for .csv datasets.
    :param input_sep: the delimiter to use. Only for .csv datasets.
    :param output_path:  output file path: str.
    :param cols_to_delete: a list of columns to delete. Only for .csv datasets.
    :param clean_options: a list of elements to remove from the tweets. Possible values:
    p.OPT.URL,p.OPT.MENTION, p.OPT.HASHTAG,p.OPT.RESERVED,p.OPT.EMOJI,p.OPT.SMILEY,p.OPT.NUMBER
    :param new_columns: new columns that will be created in the dataset, that are derived from tokens. Only for .csv datasets.

    :return: Returns the file path of the cleaned file.
    :rtype: string
    Usage::
          import twpreprocessor
          cleaned_tweet = twpreprocessor.clean_dataset("test.csv",";","text","my_cleaned_file.csv")
    Type:      function
    """
    if new_columns is None:
        new_columns: Optional[list[str]] = [p.OPT.URL, p.OPT.MENTION, p.OPT.HASHTAG, p.OPT.RESERVED, p.OPT.EMOJI,
                                            p.OPT.SMILEY,
                                            p.OPT.NUMBER]
    if clean_options is None:
        clean_options: Optional[list[str]] = [p.OPT.URL, p.OPT.MENTION,
                                              p.OPT.HASHTAG, p.OPT.RESERVED,
                                              p.OPT.EMOJI, p.OPT.SMILEY,
                                              p.OPT.NUMBER]
    if not output_path:
        vect_inp_path = input_path.split(os.sep)
        output_path:str = os.sep.join(vect_inp_path[:-1])+os.sep+"preprocessed_" + vect_inp_path[-1]

    p.set_options(*clean_options)

    if input_path.lower().endswith('.csv'):
        df = pd.read_csv(input_path, sep=input_sep)
        df.dropna(subset=[column_name], inplace=True)
        if cols_to_delete:
            for col in cols_to_delete:
                if col in df.columns:
                    df.drop(columns=[col], inplace=True)
        for new_col_name in new_columns:
            df[new_col_name] = " "
        for i, row in df.iterrows():
            parsed_tweet = p.parse(row[column_name])
            fill_row(row, parsed_tweet, new_columns)
            row[column_name] = p.clean(row[column_name])
        df.to_csv(output_path, sep=input_sep, index=False)

    elif input_path.lower().endswith('.txt'):
        output: str = p.clean_file(input_path)
        os.rename(output, output_path)
    else:
        print("File not supported. Only '.csv' and '.txt' files are supported.")
        return ""

    return output_path
