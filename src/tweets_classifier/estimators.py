import copy
import os
import pickle
from typing import Optional

import numpy as np
import pandas as pd
from joblib import dump, load
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC

from tweets_classifier.clean_data import clean_dataset
from tweets_classifier.encoder import (text2arr, to_one_hot_fit_transform, to_ordinal_fit_transform, from_ordinal,
                     to_one_hot_transform, to_ordinal_transform)
from tweets_classifier.metrics import store_metrics
from tweets_classifier.model_wrapper import Model


def ini_classification_models(model_names:Optional[list[str]] = None, random_state: int = 123) -> list[Model]:
    if model_names is None:
        model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier',
                                  'GaussianNB','GradientBoostingClassifier']
    models: list[Model] = []
    for name in model_names:
        if name == "11-NN":
            models.append(Model(name, KNeighborsClassifier(n_neighbors=11)))
        elif name == "MLP":
            models.append(Model(name,MLPClassifier(random_state=random_state,
                                                   hidden_layer_sizes=(13, 13, 13),
                                                   max_iter=1000,early_stopping=True)))
        elif name == "SVC":
            models.append(Model("SVC", SVC(random_state=random_state, probability=True)))
        elif name == "RandomForest":
            models.append(Model("RandomForest", RandomForestClassifier(random_state=random_state)))
        elif name == "GaussianProcessClassifier":
            models.append(Model(
                "GaussianProcessClassifier",
                GaussianProcessClassifier(kernel=1.0 * RBF(1.0), random_state=random_state)))
        elif name == "GaussianNB":
            models.append(Model("GaussianNB", GaussianNB()))

        elif name == "GradientBoostingClassifier":
            models.append(Model(
                "GradientBoostingClassifier",
                GradientBoostingClassifier(random_state=random_state)))
        else:
            raise Exception("This library does not support "+name+" model.")

    return models


def fit_models(X: np.ndarray,y: np.array, models: list[Model], random_state:int=42) -> None:
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=random_state)
    best_acc = 0
    best_model_clf = None
    best_model_name = ""
    is_train: bool = True
    for model in models:
        clf = OneVsRestClassifier(model.model)
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        store_metrics(model, y_test, y_pred, list(np.unique(y)),is_train)
        balanced_acc = balanced_accuracy_score(y_test, y_pred)
        if balanced_acc > best_acc:
            best_acc = balanced_acc
            best_model_clf = clf
            best_model_name = model.name
        # store model
        dump(clf, "tmp"+os.sep+model.name+'.joblib')
    # store best
    print(f'\nThe best classification method is {best_model_name}, which balanced accuracy is {best_acc}.')
    dump(best_model_clf, "tmp"+os.sep+'best.joblib')


def get_x_cols(df:pd.DataFrame, y_name:str)->list[str]:
    x_cols = list(df.columns)
    x_cols.remove(y_name)
    return x_cols


def predict_model(
        X: np.ndarray,
        model: Model) -> np.array:
    clf = load(model.name + '.joblib')
    y_pred = clf.predict(X)
    return y_pred


def evaluate_models(
        X: np.ndarray,
        y: np.array,
        models: list[Model]) -> np.array:
    best_acc = 0
    best_model_name = ""
    is_train: bool = False
    for model in models:
        y_pred = predict_model(X, model)
        store_metrics(model, y, y_pred, list(np.unique(y)), is_train)
        balanced_acc = balanced_accuracy_score(y, y_pred)
        if balanced_acc > best_acc:
            best_acc = balanced_acc
            best_model_name = model.name
    print(f'\nThe best classification method is {best_model_name}, which balanced accuracy is {best_acc}.')


class Cfg:
    def __init__(self, text_col:str, y_col:str, input_sep:str, other_x_cols:list[str],cols_to_delete:list[str],
                 model_names:list[str]):
        self.text_col = text_col
        self.y_col = y_col
        self.input_sep = input_sep
        self.other_x_cols = other_x_cols
        self.cols_to_delete = cols_to_delete
        self.model_names = model_names


def create_if_not_exists(name:str):
    path = os.getcwd() + os.sep + name
    if not os.path.exists(path):
        os.makedirs(path)


def train_dataset(input_path:str = "data"+os.sep+"train_p.csv", input_sep:str = ",", text_col:str="text",
                  y_col:str="sentiment", model_names: Optional[list[str]] = None,
                  other_x_cols: Optional[list[str]] = None,
                  cols_to_delete: Optional[list[str]] = None):

    create_if_not_exists("results")
    create_if_not_exists("tmp")

    if cols_to_delete is None:
        cols_to_delete = ["textID", "selected_text"]
    if other_x_cols is None:
        other_x_cols: list[str] = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
    if model_names is None:
        model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                                 'GradientBoostingClassifier']


    new_df_path = clean_dataset(input_path, text_col, input_sep, cols_to_delete=cols_to_delete)
    df: pd.DataFrame = pd.read_csv(new_df_path, sep=input_sep)
    df = text2arr(df,text_col,y_col,other_x_cols)
    df = to_one_hot_fit_transform(df, list_exclude=[y_col])
    df = to_ordinal_fit_transform(df, list_categ_cols=[y_col])
    models: list[Model] = ini_classification_models(model_names, random_state=123)
    x_cols = get_x_cols(df, y_col)

    with open("results" + os.sep + "accuracies_train.csv", 'w') as file1:
        file1.write( "time" + ";" + "algorithm" + ";" + "balanced_accuracy")

    fit_models(df[x_cols].values, df[y_col].values, models)
    mycfg = Cfg(text_col, y_col, input_sep, other_x_cols, cols_to_delete, model_names)
    with open("tmp" + os.sep + 'mycfg.pkl', 'wb') as outp:
        pickle.dump(mycfg, outp, pickle.HIGHEST_PROTOCOL)


def test_dataset(input_path: str, input_sep:str = ","):
    create_if_not_exists("results")
    create_if_not_exists("tmp")
    with open("tmp" + os.sep + 'mycfg.pkl', 'rb') as inp:
        mycfg: Cfg = pickle.load(inp)

    new_df_path = clean_dataset(input_path, mycfg.text_col, input_sep, cols_to_delete=mycfg.cols_to_delete)
    df_ori: pd.DataFrame = pd.read_csv(new_df_path, sep=input_sep)
    df = text2arr(df_ori, mycfg.text_col, mycfg.y_col, mycfg.other_x_cols)
    df = to_one_hot_transform(df)
    df = to_ordinal_transform(df)

    x_cols = get_x_cols(df, mycfg.y_col)
    clf = load("tmp" + os.sep + 'best.joblib')
    y_pred = clf.predict(df[x_cols].values)
    balanced_acc = balanced_accuracy_score(df[mycfg.y_col].values, y_pred)
    print(f'The test best model score is {balanced_acc}.')

    # recover predictions in text format
    df_copy = copy.deepcopy(df)
    df_copy[mycfg.y_col] = y_pred
    recovered_pred_y = from_ordinal(df_copy)

    # store predictions into the original dataset
    df_ori['pred'] = recovered_pred_y[mycfg.y_col].values

    vect_inp_path = input_path.split(os.sep)
    output_path: str = os.sep.join(vect_inp_path[:-1])+os.sep+"predicted_" + vect_inp_path[-1]
    df_ori.to_csv(output_path, sep=input_sep, index=False)