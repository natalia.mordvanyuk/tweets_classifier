import os
import unittest

import pandas as pd
import pytest_check as check
from src.tweets_classifier.clean_data import clean_dataset


class TestCleanData(unittest.TestCase):
    def test_clean_data(self):
        text_col = "text"
        input_path = "src" + os.sep +"data" + os.sep + "train_p.csv"
        out_path = "src" + os.sep +"data" + os.sep + "preprocessed_train_p.csv"
        input_sep = ";"

        df = pd.read_csv(input_path, sep=input_sep)
        new_df_path = clean_dataset(input_path, text_col, input_sep, output_path=out_path, cols_to_delete=["textID", "selected_text"])
        new_df = pd.read_csv(new_df_path, sep=input_sep)

        check.equal(len(new_df.loc[5][text_col]) == len(df.loc[5][text_col]),False)
        check.equal(new_df.loc[5]["urls"] == "http://www.dothebouncy.com/smf", True)
        check.equal(len(new_df.columns) == len(df.columns), False)


