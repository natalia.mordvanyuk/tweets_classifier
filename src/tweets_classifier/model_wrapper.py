from dataclasses import dataclass
from typing import Optional

import numpy as np
from sklearn.base import BaseEstimator


@dataclass
class Model:
    name: str
    model: BaseEstimator
    requires_flatten: bool = True

    def __str__(self) -> str:
        return self.name

    def fit(self, X: np.ndarray, y: np.array = None) -> None:
        self.model.fit(X, y)  # pylint: disable=no-member

    def predict(self, X: np.ndarray) -> Optional[np.array]:
        # pylint: disable-next=no-member
        return self.model.predict(X)  # type:ignore [no-any-return]
