import os
import unittest

import pandas as pd
import pytest_check as check

from src.tweets_classifier.clean_data import clean_dataset
from src.tweets_classifier.encoder import to_one_hot_fit_transform, to_ordinal_fit_transform, text2arr
from src.tweets_classifier.estimators import ini_classification_models, get_x_cols, fit_models, train_dataset, \
    test_dataset
from src.tweets_classifier.model_wrapper import Model


class TestEstimators(unittest.TestCase):

    def test_ini_classification_models(self):
        models = ini_classification_models()
        check.equal(len(models) == 7, True)

    def test_fit_models(self):
        text_col = "text"
        y_col = "sentiment"
        other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
        model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                                  'GradientBoostingClassifier']
        input_path =  "src" + os.sep +"data" + os.sep + "train_p.csv"
        input_sep = ";"

        new_df_path = clean_dataset(input_path, text_col, input_sep, cols_to_delete=["textID", "selected_text"])
        df: pd.DataFrame = pd.read_csv(new_df_path, sep=input_sep)

        df = text2arr(df, text_col, y_col, other_x_cols)
        df = to_one_hot_fit_transform(df, list_exclude=[y_col])
        check.equal(df.shape[1], 377)
        df = to_ordinal_fit_transform(df, list_categ_cols=[y_col])
        check.equal(df.shape[1], 377)

        models: list[Model] = ini_classification_models(model_names, random_state=123)
        x_cols = get_x_cols(df, y_col)

        check.equal(not (y_col in x_cols), True)

        fit_models(df[x_cols].values, df[y_col].values, models)

        check.equal(os.path.exists("tmp" + os.sep + 'best.joblib'), True)

    def test_train_dataset(self):
        text_col = "text"
        y_col = "sentiment"
        other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
        model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                                  'GradientBoostingClassifier']
        input_path =  "src" + os.sep +"data" + os.sep + "train_p.csv"
        input_sep = ";"
        cols_to_delete = ["textID", "selected_text"]

        train_dataset(input_path, input_sep, text_col,y_col, model_names,other_x_cols,cols_to_delete)

        check.equal(os.path.exists("tmp" + os.sep + 'best.joblib'), True)
        check.equal(os.path.exists("tmp" + os.sep + "mycfg.pkl"), True)

    def test_test_dataset(self):
        text_col = "text"
        y_col = "sentiment"
        other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
        model_names: list[str] = ['SVC', 'MLP', '11-NN', 'RandomForest', 'GaussianProcessClassifier', 'GaussianNB',
                                  'GradientBoostingClassifier']
        input_path =  "src" + os.sep +"data" + os.sep +  "train_p.csv"
        input_sep = ";"
        cols_to_delete = ["textID", "selected_text"]

        train_dataset(input_path, input_sep, text_col, y_col, model_names, other_x_cols, cols_to_delete)

        check.equal(os.path.exists("tmp" + os.sep + 'best.joblib'), True)
        check.equal(os.path.exists("tmp" + os.sep + "mycfg.pkl"), True)

        input_path_test =  "src" + os.sep +"data" + os.sep + "test.csv"
        input_sep_test = ","
        test_dataset(input_path_test, input_sep_test)




