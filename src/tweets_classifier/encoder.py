import os
import pickle
from typing import Optional

import category_encoders as ce
import numpy as np
import pandas as pd
import spacy


def text2arr(df: pd.DataFrame, text_col:str, y_col:str,other_x_cols:list[str]) -> pd.DataFrame:

    nlp = spacy.load('en_core_web_lg')
    df.dropna(subset=[text_col],inplace=True)
    new_df_list:list = []
    text_vector:np.array = np.array([])

    for i,row in df.iterrows():
        text_vector = nlp(row[text_col]).vector
        new_df_list.append(np.hstack([text_vector, row[other_x_cols], [row[y_col]]]))

    try:
        cols = ["dim_"+str(i) for i in range(text_vector.shape[0])]
        cols += other_x_cols
        cols += [y_col]
        new_df = pd.DataFrame(new_df_list, columns=cols)
        return new_df
    except:
        raise Exception("Empty dataframe.")


def quit_underscore_from_columns(df: pd.DataFrame) -> None:
    dict_of_cols: dict[str, str] = {}
    for col in df.columns:
        dict_of_cols[col] = col.replace("_", "-")
    df.rename(columns=dict_of_cols, inplace=True)


def get_cols_dict(df: pd.DataFrame, list_categ_cols: list[str]) -> dict[str, list[str]]:
    dict_of_cols = {}
    df_cols: list[str] = list(df.columns)
    for col in list_categ_cols:
        for c in df_cols:
            vct: list[str] = c.split("_")
            if len(vct) > 1 and vct[0] == col:
                dict_of_cols[c] = vct
    return dict_of_cols


def to_one_hot_fit_transform(
        df_train: pd.DataFrame,
        list_categ_cols: Optional[list[str]] = None,
        list_exclude: Optional[list[str]] = None,
) -> pd.DataFrame:
    df_train_copy = df_train.copy(deep=True)

    quit_underscore_from_columns(df_train_copy)

    if list_categ_cols is None:
        list_categ_cols = list(df_train_copy.select_dtypes(include=["object"]).columns)

    if list_exclude is not None:
        list_categ_cols = [col for col in list_categ_cols if col not in list_exclude]

    ce_one_hot: ce.OneHotEncoder = ce.OneHotEncoder(
        cols=list_categ_cols, handle_missing="return_nan", use_cat_names=True
    )
    df_train_copy_new: pd.DataFrame = ce_one_hot.fit_transform(df_train_copy)

    assert list_categ_cols is not None
    new_list_categorical: list[str] = list(
        get_cols_dict(df_train_copy_new, list_categ_cols).keys()
    )

    with open('tmp'+os.sep+'list_categ_cols.pkl', 'wb') as outp:
        pickle.dump(list_categ_cols, outp, pickle.HIGHEST_PROTOCOL)

    with open('tmp'+os.sep+'new_list_categorical.pkl', 'wb') as outp:
        pickle.dump(new_list_categorical, outp, pickle.HIGHEST_PROTOCOL)

    with open('tmp'+os.sep+'ce_one_hot.pkl', 'wb') as outp:
        pickle.dump(ce_one_hot, outp, pickle.HIGHEST_PROTOCOL)

    return df_train_copy_new


def to_one_hot_transform(
        df_test: pd.DataFrame
) -> pd.DataFrame:
    with open('tmp'+os.sep+'ce_one_hot.pkl', 'rb') as inp:
        ce_one_hot: ce.OneHotEncoder = pickle.load(inp)

    df_test_copy = df_test.copy(deep=True)
    quit_underscore_from_columns(df_test_copy)
    return ce_one_hot.transform(df_test_copy)


def joint_columns(df: pd.DataFrame, dict_of_cols: dict[str, list[str]]) -> pd.DataFrame:
    for column_to_drop in dict_of_cols:
        if len(dict_of_cols[column_to_drop]) > 1:
            new_col: str = dict_of_cols[column_to_drop][0]
            new_val: str = dict_of_cols[column_to_drop][1]
            if not new_col in df.columns:
                df[new_col] = np.NaN
            df.loc[df[column_to_drop] == 1, new_col] = new_val
            df = df.drop(columns=[column_to_drop])
    return df


def from_one_hot(
        df: pd.DataFrame
) -> pd.DataFrame:
    df_copy: pd.DataFrame = df.copy(deep=True)

    with open('tmp'+os.sep+'list_categ_cols.pkl', 'rb') as inp:
        list_categ_cols: list[str] = pickle.load(inp)

    dict_of_cols: dict[str, list[str]] = get_cols_dict(df_copy, list_categ_cols)

    df_copy = joint_columns(df_copy, dict_of_cols)

    return df_copy


def to_ordinal_fit_transform(
        df_train: pd.DataFrame,
        list_categ_cols: Optional[list[str]] = None,
        list_exclude: Optional[list[str]] = None,
) -> pd.DataFrame:
    if list_categ_cols is None:
        list_categ_cols = list(df_train.select_dtypes(include=["object"]).columns)

    if list_exclude:
        list_categ_cols = [col for col in list_categ_cols if col not in list_exclude]

    ce_ord: ce.OrdinalEncoder = ce.OrdinalEncoder(cols=list_categ_cols, handle_missing="return_nan")
    df_train_copy_new: pd.DataFrame = ce_ord.fit_transform(df_train)
    for col in list_categ_cols:
        df_train_copy_new[col] = df_train_copy_new[col].fillna(0).astype(int)

    try:
        with open('tmp'+os.sep+'list_categ_cols_ord.pkl', 'wb') as outp:
            pickle.dump(list_categ_cols, outp, pickle.HIGHEST_PROTOCOL)

        with open('tmp'+os.sep+'ce_ord.pkl', 'wb') as outp:
            pickle.dump(ce_ord, outp, pickle.HIGHEST_PROTOCOL)
    except Exception as e:
        print(str(e))

    return df_train_copy_new


def to_ordinal_transform(
        df_test: pd.DataFrame
) -> pd.DataFrame:
    with open('tmp'+os.sep+'ce_ord.pkl', 'rb') as inp:
        ce_ord: ce.OrdinalEncoder = pickle.load(inp)

    with open('tmp'+os.sep+'list_categ_cols_ord.pkl', 'rb') as inp:
        list_categ_cols_ord: list[str] = pickle.load(inp)

    df_test_copy_new: pd.DataFrame = ce_ord.transform(df_test)
    for col in list_categ_cols_ord:
        df_test_copy_new[col] = df_test_copy_new[col].fillna(0).astype(int)

    return df_test_copy_new


def from_ordinal(
        df_train: pd.DataFrame
) -> pd.DataFrame:
    with open('tmp'+os.sep+'ce_ord.pkl', 'rb') as inp:
        ce_ord: ce.OrdinalEncoder = pickle.load(inp)

    with open('tmp'+os.sep+'list_categ_cols_ord.pkl', 'rb') as inp:
        list_categ_cols_ord: list[str] = pickle.load(inp)

    for col in list_categ_cols_ord:
        df_train[col] = df_train[col].astype(np.float64)

    return ce_ord.inverse_transform(df_train)