import unittest

import os
import pandas as pd
import numpy as np
import pytest_check as check

from src.tweets_classifier.clean_data import clean_dataset
from src.tweets_classifier.encoder import to_one_hot_fit_transform,from_one_hot,to_ordinal_fit_transform,from_ordinal,text2arr


class TestOneHotEncoder(unittest.TestCase):

    def test_one_hot_encoder(self):
        text_col = "text"
        input_path = "src" + os.sep +"data" + os.sep + "train_p.csv"
        out_path = "src" + os.sep +"data" + os.sep + "preprocessed_train_p.csv"
        input_sep = ";"
        y_col = "sentiment"

        new_df_path = clean_dataset(input_path, text_col, input_sep, output_path=out_path, cols_to_delete=["textID", "selected_text"])
        new_df = pd.read_csv(new_df_path, sep=input_sep)
        new_df_oh = to_one_hot_fit_transform(new_df, list_exclude=[y_col])

        check.equal(len(new_df.columns) == len(new_df_oh.columns), False)

        new_df_from_oh = from_one_hot(new_df_oh)

        check.equal(len(new_df.columns) == len(new_df_from_oh.columns), True)

    def test_to_ordinal_fit_transform(self):
        text_col = "text"
        input_path = "src" + os.sep +"data" + os.sep + "train_p.csv"
        out_path = "src" + os.sep +"data" + os.sep + "preprocessed_train_p.csv"
        input_sep = ";"
        y_col = "sentiment"

        new_df_path = clean_dataset(input_path, text_col, input_sep, output_path=out_path, cols_to_delete=["textID", "selected_text"])
        new_df = pd.read_csv(new_df_path, sep=input_sep)
        new_df_o = to_ordinal_fit_transform(new_df, list_categ_cols=[y_col])

        check.equal(new_df_o[y_col].dtype==np.int32, True)

        new_df_from_o = from_ordinal(new_df_o)

        check.equal(new_df_from_o[y_col].dtype==object, True)

    def test_text2arr(self):
        text_col = "text"
        y_col = "sentiment"
        other_x_cols = ['urls', 'mentions', 'hashtags', 'reserved_words', 'emojis', 'smileys', 'numbers']
        train_path = "src" + os.sep +"data" + os.sep + "train_p.csv"
        input_sep = ";"

        new_df_path = clean_dataset(train_path, text_col, input_sep, cols_to_delete=["textID", "selected_text"])
        new_df = pd.read_csv(new_df_path, sep=input_sep)
        new_df = text2arr(new_df,text_col,y_col,other_x_cols)
        check.equal(len(new_df.columns), 300+len(other_x_cols)+1)
